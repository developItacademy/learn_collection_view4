//
//  DetailViewController.swift
//  LearnCollectionView
//
//  Created by Steven Hertz on 8/15/19.
//  Copyright © 2019 DevelopItSolutions. All rights reserved.
//

import UIKit
import CloudKit
import UserNotifications

enum Literals: Int {
    case nbrOfDays = 5
    
    static func nbrOfDaysInt() -> Int {
        Literals.nbrOfDays.rawValue
    }
}



class SpinnerViewController: UIViewController {
    var spinner = UIActivityIndicatorView(style: .whiteLarge)
    
    override func loadView() {
        view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.7)
        
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.startAnimating()
        view.addSubview(spinner)
        
        spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}

class DetailViewController: UIViewController {
    
    // MARK: - properties of the ViewController
    
    // passed in the segue
    var uuid: String = ""
    var theItem: String = ""
    var theStudent: User!
    var thePointerInArray: Int!
    
    var ipadID : String = ""
    
    var workWithStudentArrayDelegate: WorkWithStudentArray!
    
    var profileForTheDayArray   = Array(repeating: String(), count: Literals.nbrOfDaysInt())
    var setAlready: Bool = false
    
    var dbs : CKDatabase {
        return CKContainer(identifier: "iCloud.com.dia.cloudKitExample.open").publicCloudDatabase
    }
    
    @IBOutlet weak var studentImageView: UIImageView!

    // MARK: - View controller life cycle functions
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let thePointerInArray = workWithStudentArrayDelegate.getStudentLocationInArray(student: theStudent)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureStudent()
    }

    // MARK: - React To The Screen
    
    
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        
        let child = SpinnerViewController()
        // add the spinner view controller
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)

        let tappedImage = tapGestureRecognizer.view as! UIImageView
        tappedImage.isUserInteractionEnabled = false
        
        getUserDetail(with: String(theStudent.id),tappedImage, child)
        
    }
    


    // MARK: - Helper Setup Functiond
    
    fileprivate func configureStudent() {
//        print("```* * * * * * *", theItem)
//        let studentInfoArray = AppData.itemsData[theItem]
//        print("```* * * * *", studentInfoArray?.first)
        studentImageView.layer.cornerRadius = 160
        studentImageView.layer.masksToBounds = true
        studentImageView.image = UIImage(named: theStudent.username)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        studentImageView.isUserInteractionEnabled = true
        studentImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    // MARK: - Helper React to Click Functions
    
    fileprivate func createLoginRecord(_ levl: String) {
        
        /// Instantiate a CKRecord with the CKRecordID
        let record = CKRecord(recordType: "Logins")
        
        /// populate the 2 fields
        record["iPadID"] = ipadID as NSString
        record["level"] = levl as NSString
        record["student"] = theItem as NSString
        
        
        /// save it
        dbs.save(record) { [unowned self] (record, error) in
            print("```* - * - Saving . . .")
            DispatchQueue.main.async {
                if let error = error {
                    print("```* - * - error saving it \(error)")
                } else {
                    print("```* - * - succesful ***")
                    print(record as Any)
                }
            }
        }
    }
    
    func getUserDetail(with userID: String, _ tappedImage: UIImageView, _ child: SpinnerViewController) {
        GetDataApi.getUserDetail(GeneratedReq.init(request: ValidReqs.userDetail(userId: userID))) { (usrdtl) in
            guard let usrdtlResponse = usrdtl as? UserDetailResponse else {fatalError("could not convert it to devicedetail")}
            
            DispatchQueue.main.async {
                print("*** Hooray printed user detail-- well done")
                print("got user, the name is \(usrdtlResponse.user.firstName + " " + usrdtlResponse.user.lastName)")
                self.theStudent = usrdtlResponse.user
                self.doStuffDependentOnNoteProperty(tappedImage, child)
            }
        }
    }
    
    fileprivate func doStuffDependentOnNoteProperty(_ tappedImage: UIImageView, _ child: SpinnerViewController) {

        self.processStudentNotes()
        
        let levl = profileForTheDayArray[getDayOfWeekNbr()]
        moveiPadIntoDeviceGroup(levl)
        
        /**  Send out Notification */
        let nextStudent = workWithStudentArrayDelegate.getNextStudent(pointerToCurrentStudent: thePointerInArray)
        HelperFunctions.NotificationsHelper.sendNotification(for: nextStudent)
        
        updateIpadRecord(levl)
        
        createLoginRecord(levl)
        
        unLockAndLeave(tappedImage, child)
    }

    
    fileprivate func updateIpadRecord(_ levl: String) {
        /**
         I want to update the iPad record for this iPad identifier
         */
        
        
        /// Get the iPad record with the iPad Identifier not the UUID
        let id = CKRecord.ID(recordName: ipadID)
        
        /// Get the iPad Record using the record id
        dbs.fetch(withRecordID: id) { (record, error) in
            
            guard error == nil, let record = record  else { fatalError("------------------------- Error reading the iPad record \(error?.localizedDescription  ??  "Dummy ERROR")") }
            
            ///  Update the Record with new information
            record["userLevel"] = levl as NSString
            record["currentUser"] = self.theItem as NSString
            
            
            
            
            ///  Save the record
            self.dbs.save(record) { (_, error) in
                if error != nil {
                    print("```----- error savig the record \(error?.localizedDescription ?? "Dummy Error")")
                } else {
                    print("```----- Hooray I created a new record")
                }
            }
        }
    }
    
    fileprivate func moveiPadIntoDeviceGroup(_ levl: String) {

        // print("```Selected \(String(describing: AppData.itemsData[theItem]))")
        
        GetDataApi.updateNoteProperty(GeneratedReq.init(request: ValidReqs.updateDeviceProperty(deviceId: ipadID, propertyName: "notes", propertyValue: levl))) {
            DispatchQueue.main.async {
                print("```*** Updated the notes property of this iPad - Hooray Job well done")
            }
        }
    }
    
    
    func processStudentNotes() {
        /// Setup
        let delimeter = "~#~"
        
        /// Function Call
        do {
            let (extractedString, cleanString) = try HelperFunctions.getStringFrom(passedString: theStudent.notes, using: delimeter)
            
             setAlready = true
            
            /// Process Cleaned String
            print(cleanString)
            
            /// Process Extracted  String
            let studentsNotesAppProfileArray = String(extractedString).split(separator: ";")
            
            /// Load Profiles
            for (idx, item)  in studentsNotesAppProfileArray.enumerated() {
                profileForTheDayArray[idx] = (String(item))
            }
        }
        catch  {
             setAlready = false
            return
        }
    }
    
    func getDayOfWeekNbr() -> Int {
        let dayArray = [ "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"  ]


        let dateFormater = DateFormatter()

        dateFormater.locale = Locale(identifier: "en-US")
        dateFormater.setLocalizedDateFormatFromTemplate("EEEE")

        /// Get the text day ie Sunday, Monday,
        let x = dateFormater.string(from: Date())

        let numDate = dayArray.firstIndex(of: x)!

        print(dayArray.firstIndex(of: x)!)

        return dayArray.firstIndex(of: x)!

    }
    
 
    fileprivate func unLockAndLeave(_ tappedImage: UIImageView, _ child: SpinnerViewController) {
        /**
         Take off the spinner and close up
         */
        DispatchQueue.main.asyncAfter(deadline: .now() + 9.0) {
            tappedImage.isUserInteractionEnabled = true
            
            child.willMove(toParent: nil)
            child.view.removeFromSuperview()
            child.removeFromParent()
            
            UIApplication.shared.perform(#selector(NSXPCConnection.suspend))
        }
    }

}



